package com.practice;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        ArrayList<Book> arr = new ArrayList();
        String isoDatePattern = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat(isoDatePattern);
        Book b1 = new Book("Begining Java",1,"Ivor Horton",94948,dateFormat.parse("2022-07-23",new ParsePosition(0)));
        Book b2 = new Book("Head First Java", 2, "Sierra & bates", 23257,dateFormat.parse("2022-07-23",new ParsePosition(0)));
        Book b3 = new Book("Gitanjali", 3, "Rabindranath", 76578,dateFormat.parse("2022-07-23",new ParsePosition(0)));
        Book b4 = new Book("Odyssey",4,"Homer",76579,dateFormat.parse("2022-07-23",new ParsePosition(0)));
        Book b5 = new Book("Hamlet", 5, "Shakespeare", 57344, dateFormat.parse("2022-07-23", new ParsePosition(0)));

        arr.add(b1);
        arr.add(b2);
        arr.add(b3);
        arr.add(b4);
        arr.add(b5);
        for(int i=0; i<arr.size(); i++){

            System.out.println("Book name: "+arr.get(i).getBookName()+"    Published date: "+dateFormat.format(arr.get(i).getPublishedDate())+"    Author name: "+arr.get(i).getBookAuthor());
        }
    }
}
