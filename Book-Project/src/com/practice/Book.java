package com.practice;

import java.util.Date;

public class Book {
    private String bookName;
    private int bookID;
    private String bookAuthor;
    private int ISBN;
    private Date publishedDate;

    public Book(String bookName, int bookID, String bookAuthor, int ISBN, Date publishedDate) {
        this.bookName = bookName;
        this.bookID = bookID;
        this.bookAuthor = bookAuthor;
        this.ISBN = ISBN;
        this.publishedDate = publishedDate;
    }

    public Book() {
    }

    public String getBookName() {
        return bookName;
    }

    public int getBookID() {
        return bookID;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public int getISBN() {
        return ISBN;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }
}
